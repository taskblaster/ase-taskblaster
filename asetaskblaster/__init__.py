from htwutil.repository import Repository
from htwutil.storage import JSONCodec


class ASECodec(JSONCodec):
    def encode(self, obj):
        from ase.io.jsonio import default
        return default(obj)

    def decode(self, dct):
        from ase.io.jsonio import object_hook
        return object_hook(dct)




# MPI / gpaw awareness
# List of modules providing tasks, workflows (code execution whitelists)
def tb_init_repo(root):
    return Repository(root, usercodec=ASECodec())
