from setuptools import setup, find_packages

setup(name='ase-taskblaster',
      version='0.1',
      description='ASE/TaskBlaster integration',
      packages=find_packages())
